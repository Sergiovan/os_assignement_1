/* 
 * Operating Systems  (2INC0)  Practical Assignment
 * Interprocess Communication
 *
 * STUDENT_NAME_1 0867048 Sergio van Passel
 * STUDENT_NAME_2 0870280 Bogdan Raducanu
 *
 * Grading:
 * Students who hand in clean code that fully satisfies the minimum requirements will get an 8. 
 * ”Extra” steps can lead to higher marks because we want students to take the initiative. 
 * Extra steps can be, for example, in the form of measurements added to your code, a formal 
 * analysis of deadlock freeness etc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>          // for perror()
#include <unistd.h>         // for getpid()
#include <mqueue.h>         // for mq-stuff
#include <time.h>           // for time()
#include <complex.h>

#include "settings.h"
#include "common.h"

static void rsleep(int x);

static double 
complex_dist (complex a)
{
    // distance of vector 'a'
    // (in fact the square of the distance is computed...)
    double re, im;
    
    re = __real__ a;
    im = __imag__ a;
    return ((re * re) + (im * im));
}

static int 
mandelbrot_point (double x, double y)
{
    int     k;
    complex z;
	complex c;
    
	z = x + y * I;     // create a complex number 'z' from 'x' and 'y'
	c = z;

	for (k = 0; (k < MAX_ITER) && (complex_dist (z) < INFINITY); k++)
	{
	    z = z * z + c;
    }
    
    //                                    2
    // k >= MAX_ITER or | z | >= INFINITY
    
    return (k);
}


int main (int argc, char * argv[])
{
    mqd_t   farmer_req;
    mqd_t   worker_snd;
    WORKER_DATA     start_point;
    FARMER_DATA     line;

    // open farmer to worker queue
    farmer_req = mq_open (argv[0], O_RDONLY);
    if(farmer_req < 0){
        perror("Error opening queue");
    }
    
    // open worker to farmer queue
    worker_snd = mq_open (argv[1], O_WRONLY);
    if(worker_snd < 0){
        perror("Error opening queue");
    }
    
    do {
        mq_receive (farmer_req, (char *) &start_point, sizeof (start_point), NULL);
        if(start_point.ycoord >= 0){ //Farmer will send ycoord -1 to terminate workers
            rsleep(10000);
            int x;
            line.ycoord = start_point.ycoord;
            for(x = 0; x < X_PIXEL; x++) {
                /* Calculate line, it's our job */
                line.colors[x] = mandelbrot_point(X_LOWERLEFT + STEP * (double) x,Y_LOWERLEFT + STEP * (double) start_point.ycoord);
            }
            if(mq_send (worker_snd, (char *) &line, sizeof (line), 0) < 0){
                perror("Error sending data through queue");
            }
        }
    } while(!start_point.finish);  //If we are not told to finish, keep going
    
    // close queues
    mq_close (farmer_req);
    mq_close (worker_snd);   
    
    exit(0);
}

/*
 * rsleep(int t)
 *
 * The calling thread will be suspended for a random amount of time
 * between 0 and t microseconds
 * At the first call, the random generator is seeded with the current time
 */
static void rsleep (int t)
{
    static bool first_call = true;
    
    if (first_call == true)
    {
        srandom (time(NULL) % getpid());
        first_call = false;
    }
    usleep (random () % t);
}


