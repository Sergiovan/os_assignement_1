/* 
 * Operating Systems  (2INC0)  Practical Assignment
 * Interprocess Communication
 *
 * Contains definitions which are commonly used by the farmer and the workers
 *
 * STUDENT_NAME_1 0867048 Sergio van Passel
 * STUDENT_NAME_2 0870280 Bogdan Raducanu
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include "settings.h"

typedef struct {
    int ycoord;
    int finish;
} WORKER_DATA;

typedef struct {
    int ycoord;
    int colors[X_PIXEL];
} FARMER_DATA;


#endif

