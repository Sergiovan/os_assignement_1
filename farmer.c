/* 
 * Operating Systems  (2INC0)  Practical Assignment
 * Interprocess Communication
 *
 * STUDENT_NAME_1 0867048 Sergio van Passel
 * STUDENT_NAME_2 0870280 Bogdan Raducanu
 *
 * Grading:
 * Students who hand in clean code that fully satisfies the minimum requirements will get an 8. 
 * ”Extra” steps can lead to higher marks because we want students to take the initiative. 
 * Extra steps can be, for example, in the form of measurements added to your code, a formal 
 * analysis of deadlock freeness etc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>    
#include <unistd.h>         // for execlp
#include <mqueue.h>         // for mq

#include "settings.h"
#include "output.h"
#include "common.h"

#define STUDENT_NAME_1 "Sergio_van_Passel"
#define STUDENT_NAME_2 "Bogdan_Raducanu"

static char FARMER_WORKER_QUEUE[80];
static char WORKER_FARMER_QUEUE[80];

mqd_t open_queue(const char *, struct mq_attr * att, int); //Includes error checking
void fill_send_queue(mqd_t, int * );
void empty_receive_queue(mqd_t, int *);


int main (int argc, char * argv[])
{
    if (argc != 1)
    {
        fprintf (stderr, "%s: invalid arguments\n", argv[0]);
        exit(1);
    }
    
    output_init ();

    mqd_t to_worker_queue; //Queue to send data to workers
    mqd_t from_worker_queue; //Queue to receive data from workers
    pid_t processID;
    struct mq_attr attr;
    
    sprintf(FARMER_WORKER_QUEUE, "/mq_queue_out_%d_%s", getpid(), STUDENT_NAME_1);
    sprintf(WORKER_FARMER_QUEUE, "/mq_queue_in_%d_%s", getpid(), STUDENT_NAME_2);
    
    attr.mq_maxmsg = MQ_MAX_MESSAGES; //Forming the queue's attributes according to settings.h
    attr.mq_msgsize = sizeof(WORKER_DATA);
    
    /* Opening queue to send data to workers */
    to_worker_queue = open_queue(FARMER_WORKER_QUEUE, &attr, 0);
    
    attr.mq_msgsize = sizeof(FARMER_DATA);
    
    /* Opening queue to receive data from workers */
    from_worker_queue = open_queue(WORKER_FARMER_QUEUE, &attr, 1);
    
    int i, j; //We'll use these later
    /* Time to create all the worker processes */
    for(i = 0; i < NROF_WORKERS; i++){
        processID = fork();
        if(processID < 0){ //This means error
            perror("Something went horribly fork()");
            exit(1);
        }else if(processID == 0){ //This is a child 
            if(execlp("./worker", FARMER_WORKER_QUEUE, WORKER_FARMER_QUEUE, NULL) == -1){ //Give child new identity
                perror("execlp() failed, we are all doomed\n");
                exit(1);
            }
        }
    }
    /* When there are more workers than work, dismiss some of them */
    if(NROF_WORKERS > Y_PIXEL){
        int gg;
        for(gg = 0; gg < (NROF_WORKERS - Y_PIXEL); gg++){
            WORKER_DATA wdat;
            wdat.ycoord = -1; //This ensures they return nothing
            wdat.finish = 1; //This ensures they finish after this job
            mq_send(to_worker_queue, (char *) &wdat, sizeof(wdat), 0);
        }
    }
    
    i = 0; //i will mark the amount of jobs sent in total
    j = 0; //j will mark the amount of results received
    while(i < Y_PIXEL){ //There is still jobs to send
        fill_send_queue(to_worker_queue, &i);
        empty_receive_queue(from_worker_queue, &j);
    }
    
    /* At this point there is only data to receive */
    while(j < Y_PIXEL){
        empty_receive_queue(from_worker_queue, &j);
    }
    
    /* Close the shop */
    mq_close(to_worker_queue);
    mq_close(from_worker_queue);
    mq_unlink(FARMER_WORKER_QUEUE);
    mq_unlink(WORKER_FARMER_QUEUE);

    output_end();
    
    return (0);
}

/**
 * Opens a queue given by mqname with attr. If rd == 1, queue is opened as read only, write only otherwise
 * Returns: mqd_t queue identifier 
 */
mqd_t open_queue(const char * mqname, struct mq_attr *attr, int rd){
    mqd_t q = mq_open(mqname, (rd ? O_RDONLY : O_WRONLY) | O_CREAT | O_EXCL, 0666, attr);
    if(q < 0){ //Error checking
        char mess[100];
        sprintf(mess, "Opening of queue %s failed", mqname);
        perror(mess);
        exit(1);
    }
    return q;
}
/**
 * Sends as much data as possible through the queue to the workers
 */
void fill_send_queue(mqd_t q,int * i){
    struct mq_attr outattr;
    mq_getattr(q, &outattr);
    while(outattr.mq_curmsgs < outattr.mq_maxmsg){ //There is space for at least one more job in the queue
        WORKER_DATA wdat;
        wdat.ycoord = (*i)++;
        wdat.finish = (*i > Y_PIXEL - NROF_WORKERS); //If we reached the last NROF_WORKERS jobs, tell workers to finish
        mq_send(q, (char *) &wdat, sizeof(wdat), 0);
        outattr.mq_curmsgs++;
    }
}

/**
 * Reads all data in the queue from the workers and prints the read data to screen
 */
void empty_receive_queue(mqd_t q, int * j){
    struct mq_attr inattr;
    mq_getattr(q, &inattr);
    int k;
    while(inattr.mq_curmsgs > 0){ //If there is any message, remove them all
        (*j)++; 
        FARMER_DATA fdat;
        mq_receive(q, (char *) &fdat, sizeof(fdat), NULL);
        for(k = 0; k < X_PIXEL; k++){
            output_draw_pixel(k, fdat.ycoord, fdat.colors[k]); //Paint the new pixels on screen from left to right
        }
        inattr.mq_curmsgs--;
    }
}
